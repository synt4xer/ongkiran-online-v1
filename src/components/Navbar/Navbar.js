import React, {useState} from 'react';
import {Img, MobileIcon, Nav, NavbarContainer, NavItem, NavLinks, NavLogo, NavMenu} from "./Navbar.elements";
import logo from '../../images/Group 3.svg';
import {FaBars, FaTimes} from "react-icons/fa";
import {IconContext} from "react-icons/lib";

const Navbar = () => {
    const [click, setClick] = useState(false);

    const handleClick = () => setClick(!click);

    return (
        <>
            <IconContext.Provider value={{ color: '#002B7F'}}>
                <Nav>
                    <NavbarContainer>
                        <NavLogo to="page1" spy={true} smooth={true} offset={-100} duration={700}>
                            <Img src={logo} alt=""/>
                        </NavLogo>
                        <MobileIcon onClick={handleClick}>
                            {click ? <FaTimes/> : <FaBars/>}
                        </MobileIcon>
                        <NavMenu onClick={handleClick} click={click}>
                            <NavItem>
                                <NavLinks activeClass="active" to='page1' spy={true} smooth={true} offset={-100}
                                          duration={700}>HOME</NavLinks>
                            </NavItem>
                            <NavItem>
                                <NavLinks activeClass="active" to='pageAPI' spy={true} smooth={true} offset={-100}
                                          duration={700}>CEK ONGKIR</NavLinks>
                            </NavItem>
                            <NavItem>
                                <NavLinks activeClass="active" to='page2' spy={true} smooth={true} offset={-100}
                                          duration={700}>ABOUT US</NavLinks>
                            </NavItem>
                        </NavMenu>
                    </NavbarContainer>
                </Nav>
            </IconContext.Provider>
        </>
    );
};

export default Navbar;
