import styled from 'styled-components'

export const InfoSec = styled.div`
  color: #fff;
  padding: 60px 0 30px;
  background: ${({lightBg}) => (lightBg ? '#FFF' : '#E9F1FA')};
  min-height: 500px;
`;

export const InfoRow = styled.div`
  display: flex;
  margin: 0 -15px -15px -15px;
  flex-wrap: wrap;
  align-items: center;
  padding-top: ${({paddingTop}) => (paddingTop ? '15px' : '0')};
  flex-direction: ${({imgStart}) => (imgStart ? 'row-reverse' : 'row')};
`;

export const InfoColumn = styled.div`
  margin-bottom: 15px;
  padding-right: 15px;
  padding-left: 15px;
  flex: 1;
  max-width: ${({maxColumn}) => (maxColumn ? '100%' : '50%')};
  flex-basis: 50%;

  @media screen and (max-width: 768px) {
    max-width: 100%;
    flex-basis: 100%;
    display: flex;
    justify-content: center;
  }
`;

export const TextWrapper = styled.div`
  max-width: 540px;
  padding-top: 50px;
  padding-left: 50px;
  padding-bottom: 50px;

  @media screen and (max-width: 768px) {
    padding-bottom: 65px;
    padding-left: 0;
  }
`;

export const Heading = styled.h1`
  margin-bottom: 24px;
  font-size: ${({textFontSize}) => textFontSize};
  line-height: 1.1;
  color: ${({lightText}) => (lightText ? '#002B7F' : '#F78234')};
  padding-left: ${({paddingLeft}) => (paddingLeft ? paddingLeft : '0')};

  @media screen and (max-width: 960px) {
    padding-left: 0;
    justify-content: center;
  }
`;

export const HeadlineText = styled.span`
  color: ${({lightText}) => (lightText ? '#F78234' : '#002B7F')};
`;

export const DetailLine = styled.p`
  max-width: 440px;
  margin-bottom: 35px;
  font-size: 18px;
  line-height: 25px;
  color: ${({lightTextDesc}) => (lightTextDesc ? '#002B7F' : '#F78234')};
`;

export const ImgWrapper = styled.div`
  max-width: 555px;
  display: flex;
  justify-content: ${({start}) => (start ? 'flex-start' : 'flex-end')};
`;

export const Img = styled.img`
  padding-right: 0;
  border: 0;
  max-width: 100%;
  vertical-align: middle;
  display: inline-block;
  max-height: 500px;
`;

export const FooterSec = styled.div`
  color: #fff;
  padding: 30px 0 40px;
  background: ${({lightBg}) => (lightBg ? '#FFF' : '#E9F1FA')};
`;

export const ContactContainer = styled.div`
  display: flex;
  margin: 0 -15px -15px -15px;
  align-items: center;
  flex-wrap: wrap;

  @media screen and (max-width: 768px) {
    padding-bottom: 65px;
    padding-left: 0;
  }
`;

export const ContactWrapper = styled.div`
  max-width: 550px;
  padding-left: 50px;
  padding-bottom: 50px;
  margin-top: 50px;

  @media screen and (max-width: 768px) {
    padding-left: 0;
  }
`;

export const ContactColumn = styled.div`
  display: inline-flex;
  margin-bottom: 15px;
  padding-left: 10px;
  flex: 1;
  max-width: 100%;
  flex-basis: 100%;

  @media screen and (max-width: 768px) {
    max-width: 100%;
    flex-basis: 100%;
    display: flex;
    justify-content: left;
  }
`;

export const ContactImg = styled.img`
  float: left;
  padding-right: 0;
  margin-bottom: 10px;
  border: 0;
  max-width: 100%;
  vertical-align: middle;
  display: inline-block;
  max-height: 50px;
`;

export const ContactTextWrapper = styled.div`
  display: inline;
  float: right;
  padding-left: 15px;
`;

export const ContactHeading = styled.h4`
  font-size: 18px;
  color: #002B7F;

  @media screen and (max-width: 960px) {
    justify-content: center;
  }
`;

export const ContactText = styled.p`
  font-size: 14px;
  color: #1A1818;

  @media screen and (max-width: 960px) {
    justify-content: center;
  }
`;
export const ApiBox = styled.div`
  background: #fff 0 0 no-repeat padding-box;
  box-shadow: 0 0 5px #1A1818BF;
  border-radius: 5px;
  opacity: 1;
  width: 100%;
  min-height: 150px;
`;

export const ApiColumn = styled.div`
  margin-bottom: ${({marginBottom}) => (marginBottom ? '0' : '15px')};
  padding-right: 15px;
  padding-left: 15px;
  display: flex;
  flex: 1;
  max-width: 100%;
  flex-basis: 50%;

  @media screen and (max-width: 768px) {
    max-width: 100%;
    flex-basis: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const ApiHeading = styled.h1`
  text-align: center;
  padding-bottom: 10px;
  margin-left: ${({paddingTop}) => (paddingTop ? '10px' : '0')};
  margin-bottom: ${({paddingTop}) => (paddingTop ? '0' : '24px')};
  padding-top: ${({paddingTop}) => (paddingTop ? '25px' : '0')};
  font-size: ${({textFontSize}) => textFontSize};
  line-height: 1.1;
  color: ${({lightText}) => (lightText ? '#002B7F' : '#F78234')};
  padding-left: ${({paddingLeft}) => (paddingLeft ? paddingLeft : '0')};

  @media screen and (max-width: 960px) {
    padding-left: 0;
    justify-content: center;
  }
`;

export const ApiMiniSpan = styled.span`
  font-size: 12px;
  color: #F78234;
  padding-left: 10px;

  @media screen and (max-width: 960px) {
    padding-left: 0;
    justify-content: center;
  }
`

export const ApiSpanHeadline = styled.span`
  color: ${({lightText}) => (lightText ? '#F78234' : '#002B7F')};
`;

export const ApiSelect = styled.select`
  width: 100%;
  height: 40px;
  background: #EDEEEF 0 0 no-repeat padding-box;
  box-shadow: 0 0 2px #1A1818BF;
  border-radius: 5px;
  opacity: 1;
  padding-left: 10px;
  margin-left: 10px;
  font-size: 14px;

  option {
    color: black;
    background: #fff;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 0 2px 1px;
  }

  @media screen and (max-width: 768px) {
    margin-bottom: 15px;
  }
`;

export const ApiButton = styled.button`
  width: 100%;
  height: 40px;
  padding: 5px 12px 0 10px;
  color: white;
  font-size: 14px;
  font-weight: 700;
  background: #F78234 0 0 no-repeat padding-box;
  border: 0;
  border-radius: 5px;
  appearance: none;
  cursor: pointer;
  opacity: 1;
  margin-left: 10px;

  @media screen and (max-width: 768px) {
    margin-bottom: 15px;
  }
`;