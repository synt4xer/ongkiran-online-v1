import React from 'react';
import {
    DetailLine,
    Heading,
    HeadlineText,
    Img,
    ImgWrapper,
    InfoColumn,
    InfoRow,
    InfoSec,
    TextWrapper
} from "./Section.elements";
import {Container} from "../../globalStyles";

const Section = ({
                     id,
                     lightBg,
                     imgStart,
                     lightText,
                     headLine,
                     nextHeadline,
                     headLine2,
                     headLine3,
                     textFontSize1,
                     textFontSize2,
                     textFontSize3,
                     lightTextDesc,
                     description,
                     start,
                     img,
                     alt
                 }) => {
    return (
        <>
            <InfoSec lightBg={lightBg} id={id}>
                <Container>
                    <InfoRow imgStart={imgStart}>
                        <InfoColumn>
                            <TextWrapper>
                                <Heading textFontSize={textFontSize1}
                                         lightText={lightText}>{headLine}<HeadlineText>{nextHeadline}</HeadlineText></Heading>
                                <Heading textFontSize={textFontSize2} lightText={!lightText}>{headLine2}</Heading>
                                <Heading textFontSize={textFontSize3} paddingLeft='180px'
                                         lightText={lightText}>{headLine3}</Heading>
                                <DetailLine lightTextDesc={lightTextDesc}>{description}</DetailLine>
                            </TextWrapper>
                        </InfoColumn>
                        <InfoColumn>
                            <ImgWrapper start={start}>
                                <Img src={img} alt={alt}/>
                            </ImgWrapper>
                        </InfoColumn>
                    </InfoRow>
                </Container>
            </InfoSec>
        </>
    );
};

export default Section;
