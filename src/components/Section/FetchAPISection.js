import React, {useCallback, useEffect, useState} from "react";
import {Container} from "../../globalStyles";
import {
    ApiBox,
    ApiButton,
    ApiColumn,
    ApiHeading,
    ApiMiniSpan,
    ApiSelect,
    ApiSpanHeadline,
    InfoColumn,
    InfoRow,
    InfoSec
} from "./Section.elements";
import Card from "../Card/Card";

const FetchAPISection = ({id, lightBg, imgStart, textFontSize, lightText, headLine, spanHeadLine}) => {
    const [province, setProvince] = useState([]);
    const [fromCity, setFromCity] = useState([]);
    const [toCity, setToCity] = useState([]);

    const [srcProvince, setSrcProvince] = useState();
    const [dstProvince, setDstProvince] = useState();
    const [srcCity, setSrcCity] = useState();
    const [dstCity, setDstCity] = useState();
    const [expedition, setExpedition] = useState();
    const [cards, setCards] = useState([]);

    useEffect(() =>
        fetch("https://api.ongkiran.online/api/provinsi")
            .then(res => res.json())
            .then(res => setProvince(res['rajaongkir'].results.map(({province_id, province}) => ({
                value: province_id,
                label: province
            })))), []);

    useEffect(() => {
        if (srcProvince != null) {
            fetch(`https://api.ongkiran.online/api/kota/${srcProvince}`)
                .then(res => res.json())
                .then(res => setFromCity(res['rajaongkir'].results.map(({city_id, city_name}) => ({
                    value: city_id,
                    label: city_name
                }))));
        }
    }, [srcProvince]);

    useEffect(() => {
        if (dstProvince != null) {
            fetch(`https://api.ongkiran.online/api/kota/${dstProvince}`)
                .then(res => res.json())
                .then(res => setToCity(res['rajaongkir'].results.map(({city_id, city_name}) => ({
                    value: city_id,
                    label: city_name
                }))))
        }
    }, [dstProvince]);

    const checkCost = useCallback(() => {
        fetch(`https://api.ongkiran.online/api/ongkos/${srcCity}/${dstCity}/1000/${expedition}`)
            .then(res => res.json())
            .then(res => setCards(res['rajaongkir'].results[0]['costs'].map(({description, service, cost}) => ({
                cardHeadline: service,
                subHeadline: description,
                amount: cost[0]['value'],
                duration: cost[0]['etd']
            }))));
    }, [srcCity, dstCity, expedition])

    return (
        <>
            <InfoSec lightBg={lightBg} id={id}>
                <Container maxContainer={true}>
                    <InfoRow imgStart={imgStart}>
                        <InfoColumn maxColumn={true}>
                            <ApiHeading textFontSize={textFontSize}
                                        lightText={lightText}>{headLine}
                                <ApiSpanHeadline lightText={lightText}>{spanHeadLine}</ApiSpanHeadline>
                            </ApiHeading>
                        </InfoColumn>
                    </InfoRow>
                    <InfoRow imgStart={imgStart}>
                        <InfoColumn maxColumn={true}>
                            <ApiBox>
                                <ApiColumn marginBottom={true}>
                                    <ApiHeading paddingTop={true} textFontSize="20px" lightText={lightText}>CEK
                                        HARGA</ApiHeading>
                                </ApiColumn>
                                <ApiColumn>
                                    <ApiMiniSpan>*Dalam ukuran 1Kg</ApiMiniSpan>
                                </ApiColumn>
                                <ApiColumn>
                                    <ApiSelect defaultValue="" onChange={e => setSrcProvince(e.currentTarget.value)}>
                                        <option value="" disabled selected hidden>Pilih Provinsi Asal</option>
                                        {
                                            province.map(
                                                ({value, label}) => (<option key={value} value={value}>{label}</option>)
                                            )
                                        }
                                    </ApiSelect>
                                    <ApiSelect defaultValue="" onChange={e => setSrcCity(e.currentTarget.value)}
                                               disabled={!srcProvince}>
                                        <option value="" disabled selected hidden>Pilih Kota Asal</option>
                                        {
                                            fromCity.map(
                                                ({value, label}) => (<option key={value} value={value}>{label}</option>)
                                            )
                                        }
                                    </ApiSelect>
                                    <ApiSelect defaultValue="" onChange={e => setDstProvince(e.currentTarget.value)}>
                                        <option value="" disabled selected hidden>Pilih Provinsi Tujuan</option>
                                        {
                                            province.map(
                                                ({value, label}) => (<option key={value} value={value}>{label}</option>)
                                            )
                                        }
                                    </ApiSelect>
                                    <ApiSelect defaultValue="" onChange={e => setDstCity(e.currentTarget.value)}
                                               disabled={!dstProvince}>
                                        <option value="" disabled selected hidden>Pilih Kota Tujuan</option>
                                        {
                                            toCity.map(
                                                ({value, label}) => (<option key={value} value={value}>{label}</option>)
                                            )
                                        }
                                    </ApiSelect>
                                    <ApiSelect defaultValue="" onChange={e => setExpedition(e.currentTarget.value)}>
                                        <option value="" disabled selected hidden>Pilih Ekspedisi</option>
                                        <option key="jne" value="jne">JNE</option>
                                        <option key="pos" value="pos">POS Indonesia</option>
                                        <option key="tiki" value="tiki">TIKI</option>
                                    </ApiSelect>
                                    <ApiButton onClick={checkCost}>Cek Harga</ApiButton>
                                </ApiColumn>
                            </ApiBox>
                        </InfoColumn>
                    </InfoRow>
                    <InfoRow paddingTop={true} imgStart={imgStart}>
                        {
                            cards.map(
                                ({cardHeadline, subHeadline, amount, duration, index}) => (
                                    <Card key={index} cardHeadline={cardHeadline} subHeadline={subHeadline}
                                          amountText={amount} durationText={duration}/>
                                )
                            )
                        }
                    </InfoRow>
                </Container>
            </InfoSec>
        </>
    );
};

export default FetchAPISection;