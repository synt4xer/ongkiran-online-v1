import {Container} from "../../globalStyles";
import CustomersLogo from "../../images/customers.svg";
import HeadphonesLogo from "../../images/headphones.svg";
import LocationLogo from "../../images/location.svg";
import messagesLogo from "../../images/messages.svg";
import {
    ContactColumn,
    ContactContainer,
    ContactHeading,
    ContactImg,
    ContactText,
    ContactTextWrapper,
    ContactWrapper,
    FooterSec,
    Heading,
    HeadlineText,
    Img,
    ImgWrapper,
    InfoColumn,
    InfoRow
} from "./Section.elements";
import React from "react";

const FooterSection = ({lightBg, imgStart, textFontSize, lightText, Headline, Headline2, start, img, alt}) => {
    return (
        <>
            <FooterSec lightBg={lightBg}>
                <Container>
                    <InfoRow imgStart={imgStart}>
                        <InfoColumn>
                            <ContactWrapper>
                                <Heading textFontSize={textFontSize} lightText={lightText}>{Headline}
                                    <HeadlineText>{Headline2}</HeadlineText></Heading>
                                <ContactContainer>
                                    <ContactColumn>
                                        <ContactImg src={HeadphonesLogo} alt=''/>
                                        <ContactTextWrapper>
                                            <ContactHeading>Support</ContactHeading>
                                            <ContactText>0811 808 090</ContactText>
                                        </ContactTextWrapper>
                                    </ContactColumn>
                                    <ContactColumn>
                                        <ContactImg src={CustomersLogo} alt=''/>
                                        <ContactTextWrapper>
                                            <ContactHeading>Sales</ContactHeading>
                                            <ContactText>0813 8092 3020</ContactText>
                                        </ContactTextWrapper>
                                    </ContactColumn>
                                    <ContactColumn>
                                        <ContactImg src={messagesLogo} alt=''/>
                                        <ContactTextWrapper>
                                            <ContactHeading>Email</ContactHeading>
                                            <ContactText>cs@ongkiran.online</ContactText>
                                        </ContactTextWrapper>
                                    </ContactColumn>
                                    <ContactColumn>
                                        <ContactImg src={LocationLogo} alt=''/>
                                        <ContactTextWrapper>
                                            <ContactHeading>Alamat</ContactHeading>
                                            <ContactText>Jl. Raya, RT.4/RW.1, Meruya Sel., Kec. Kembangan, Jakarta,
                                                Daerah Khusus Ibukota Jakarta 11650</ContactText>
                                        </ContactTextWrapper>
                                    </ContactColumn>
                                </ContactContainer>
                            </ContactWrapper>
                        </InfoColumn>
                        <InfoColumn>
                            <ImgWrapper start={start}>
                                <Img src={img} alt={alt}/>
                            </ImgWrapper>
                        </InfoColumn>
                    </InfoRow>
                </Container>
            </FooterSec>
        </>
    )
}
export default FooterSection;
