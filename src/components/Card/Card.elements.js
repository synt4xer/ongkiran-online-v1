import styled from 'styled-components';


export const Cards = styled.ul`
  background: #fff 0 0 no-repeat padding-box;
  box-shadow: 0 0 5px #1A1818BF;
  border-radius: 5px;
  opacity: 1;
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  margin: 10px;
  padding: 10px;
  min-width: 361px;
`;

export const CardItems = styled.li`
 display: flex;
 padding: 1rem;
`;

export const Card = styled.div`
 background-color: white;
 border-radius: 0.25rem;
 box-shadow: 0 20px 40px -14px rgba(0, 0, 0, 0.25);
 display: flex;
 flex-direction: column;
 overflow: hidden;

 &:hover {
  .card__image {
   filter: contrast(100%);
  }
 }
`;

export const CardContent = styled.div`
 display: flex;
 flex: 1 1 auto;
 flex-direction: column;
 padding: 1rem;
`;

export const CardTitle = styled.div`
  color: ${({colorText}) => (colorText) ? colorText : '#002B7F'};
  font-size: 1.25rem;
  font-weight: 300;
  letter-spacing: 2px;
  text-transform: uppercase;
`
export const TextWrapper = styled.div`
 margin-top: 10px;
`;

export const CardText = styled.p`
 flex: 1 1 auto;
 font-size: 1rem;
 line-height: 1.5;
 letter-spacing: 2px;
 color: #002B7F;
 font-weight: 200;
`