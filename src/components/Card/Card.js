import {CardContent, CardItems, Cards, CardText, CardTitle, TextWrapper} from "./Card.elements";

const Card = ({key, cardHeadline, subHeadline, amountText, durationText}) => {

    return (
        <>
            <Cards>
                <CardItems>
                    <CardContent>
                        <CardTitle>Service {cardHeadline}</CardTitle>
                        <CardTitle colorText={'#F78234'}>{subHeadline}</CardTitle>
                        <TextWrapper>
                            <CardText>Harga: {amountText}</CardText>
                            <CardText>Durasi: {durationText} Hari</CardText>
                        </TextWrapper>
                    </CardContent>
                </CardItems>
            </Cards>
        </>
    );
};

export default Card;