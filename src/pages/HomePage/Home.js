import React from 'react';
import Section from "../../components/Section/Section";
import {pageAbout, pageAboutUs, PageAPI, pageContactUs, pageOne} from "./Data";
import FooterSection from "../../components/Section/FooterSection";
import FetchAPISection from "../../components/Section/FetchAPISection";

const Home = () => {
    return (
        <>
            <Section {...pageOne}/>
            <FetchAPISection {...PageAPI}/>
            <Section {...pageAbout}/>
            <Section {...pageAboutUs}/>
            <FooterSection {...pageContactUs}/>
        </>
    );
};

export default Home;
