import logo1 from '../../images/Group 97.svg'
import logoAbout from '../../images/Group 177.svg'
import logoAboutUs from '../../images/Group 190.svg'
import logoContactUs from '../../images/Group 216.svg'

export const pageOne = {
    id: "page1",
    lightBg: false,
    imgStart: false,
    lightText: true,
    headLine: 'The Future Of',
    headLine2: 'Shipping',
    textFontSize1: '60px',
    textFontSize2: '60px',
    lightTextDesc: true,
    start: '',
    img: logo1,
    alt: 'image'
};

export const PageAPI = {
    id: "pageAPI",
    lightBg: true,
    imgStart: false,
    textFontSize: '40px',
    lightText: true,
    headLine: 'Temukan harga terbaik ',
    spanHeadLine: 'DISINI'
};

export const pageAbout = {
    id: "page2",
    lightBg: false,
    imgStart: false,
    lightText: false,
    headLine: 'TENTANG',
    headLine2: 'ongkiran',
    headLine3: '.online',
    textFontSize1: '30px',
    textFontSize2: '70px',
    textFontSize3: '35px',
    lightTextDesc: true,
    start: '',
    img: logoAbout,
    alt: 'image'
}

export const pageAboutUs = {
    lightBg: true,
    imgStart: true,
    lightText: false,
    headLine: 'TENTANG ',
    nextHeadline: 'KAMI',
    description: 'Ongkiran.online didirikan pada tahun 2020 untuk menjadi jawaban bagi kebutuhan bisnis di industri penjualan online dan pengiriman barang. Ongkiran.online hadir untuk mempermudah bagi pelaku bisnis toko online dengan menyediakan daftar harga pengiriman barang. Data yang kami gunakan adalah data yang akurat dan sesuai dari penyedia layanan pengiriman yang ada di Indonesia. Ongkiran.online optimis dapat menyediakan layanan yang aman dan andal dengan solusi elektronik yang dapat membantu para pelaku bisnis toko online mudah untuk mendapatkan harga terkait pengiriman barang.',
    textFontSize1: '40px',
    textFontSize2: '40px',
    lightTextDesc: true,
    start: '',
    img: logoAboutUs,
    alt: 'image'
}

export const pageContactUs = {
    lightBg: true,
    imgStart: false,
    textFontSize: '40px',
    lightText: false,
    Headline: 'TEMUKAN ',
    Headline2: 'KAMI',
    start: '',
    img: logoContactUs,
    alt: ''
};
